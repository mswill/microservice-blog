routes:

    public:
        getPosts /public/posts
        getPostByID /public/post:id
    private:
        createPost /private/post
        deletePostByID /private/delete-post:id
        updatePostById /private/update-post:id
    notfound:
        noFoundPost /notfound-post
