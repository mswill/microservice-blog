package main

import (
	"api/internal/service_api"
	"api/pkg/db/mongodb"
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/segmentio/kafka-go"
	"log"
	"sync"
	"time"
)

func main() {
	// kafka
	kConn := service_api.StartKafka()
	// wait group
	wg := sync.WaitGroup{}

	// app instance
	app := fiber.New()

	// monitor
	app.Get("/dash", monitor.New())

	// middleware
	app.Use(logger.New())
	app.Use(compress.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Accept-Language, Content-Length, Authorization, Access-Control-Allow-Origin",
	}))
	app.Use(recover.New())
	app.Static("/static", "./uploads")

	// Server
	appServer := service_api.Server{}

	// connect to mongo
	client := mongodb.ConnectToMongo()

	repo := service_api.NewRepository(client)
	service := service_api.NewService(repo)
	handler := service_api.NewHandler(service, kConn)

	handler.PublicRoutes(app)
	handler.PrivateRoutes(app)
	handler.NotFoundRoute(app)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {

		appServer.Run(app, wg)
		wg.Done()
	}(&wg)

	wg.Wait()
}
func StartKafka() {
	fmt.Println("StartKafka ---------------------> ")
	chanCon := make(chan *kafka.Conn)
	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		conn, err := kafka.DialLeader(context.Background(), "tcp", "kafka:9092", "sendFileTopic", 1)
		if err != nil {
			log.Fatal("kafka.DialLeader --------> ", err)
		}
		fmt.Println(conn)
		chanCon <- conn
	}()

	wg.Add(1)
	time.Sleep(time.Second / 2)
	go func() {
		defer wg.Done()
		conn := <-chanCon
		msg := kafka.Message{
			Topic:     "sendFileTopic",
			Partition: 1,
			Key:       []byte("file"),
			Value:     []byte("hello from SERVICE API"),
		}

		fmt.Println(msg)
		err := conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
		if err != nil {
			fmt.Println("kafka SetWriteDeadline --------> ", err)
		}
		messages, err := conn.WriteMessages(msg)
		fmt.Println("---------- > msgs ", messages)
		if err != nil {
			fmt.Println("kafka WriteMessages -------->  ", err)
		}
		fmt.Println("Bytes written in msg  --------> ", messages)

	}()

	wg.Wait()
}
