package service_api

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"strings"
)

const (
	signKey = "tokenSecretdjkfhgfdg"
)

type AuthorFromCtx struct {
	Id       string  `json:"id" bson:"_id"`
	Email    string  `json:"email"`
	Name     string  `json:"name"`
	Exp      float64 `json:"exp"`
	IsLogged bool    `json:"isLogged"`
}

func (h Handler) ParseToken(ctx *fiber.Ctx) error {
	Logg("middleware", "ParseToken")

	token := strings.Split(ctx.Get("Authorization"), " ")[1]

	parse, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(("Invalid Signing Method "))
		}
		return []byte(signKey), nil
	})

	if claims, ok := parse.Claims.(jwt.MapClaims); ok && parse.Valid {

		a := AuthorFromCtx{
			Id:       claims["id"].(string),
			Email:    claims["email"].(string),
			Name:     claims["name"].(string),
			Exp:      claims["exp"].(float64),
			IsLogged: claims["isLogged"].(bool),
		}

		ctx.Context().SetUserValue("user", &a)
	} else {
		fmt.Println(err)
		return err
	}
	if err != nil {
		fmt.Println(err)
		return err
	}

	_ = ctx.Next()

	return nil
}
