package service_api

import "context"

type IService interface {
	CreatePost(ctx context.Context, post *Post) (string, error)
	GetPostByID(ctx context.Context, id string) (*Post, error)
	GetPosts(ctx context.Context) (*[]Post, error)
	GetAuthorPosts(ctx context.Context, aId string) (*[]Post, error)
	DeletePostByID(ctx context.Context, id int) (string, error)
	UpdatePostByID(ctx context.Context, id string, post *Post) (string, error)
	UploadFile(ctx context.Context, filePath, postId, authorID string) (string, error)
}
