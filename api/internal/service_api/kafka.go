package service_api

import (
	"context"
	"github.com/segmentio/kafka-go"
	"log"
	"os"
)

var (
	KafkaTopics = []string{"sendFileTopic"}
	KafkaHost   = os.Getenv("KAFKA_HOST")
)

func StartKafka() *kafka.Conn {

	conn, err := kafka.DialLeader(context.Background(), "tcp", KafkaHost, KafkaTopics[0], 0)
	if err != nil {
		log.Fatal(err)
	}

	return conn
}
