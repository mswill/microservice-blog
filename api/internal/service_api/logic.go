package service_api

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
)

var _ IService = &Service{}

var (
	ErrFieldsIsEmpty = errors.New("all field must be minimum 3 symbols")
)

type Service struct {
	repo IRepository
}

func NewService(repo IRepository) *Service {
	return &Service{repo: repo}
}

// CreatePost Done!
func (s *Service) CreatePost(ctx context.Context, post *Post) (string, error) {
	Logg("logic", "CreatePost")

	v := validator.New()
	err := v.Struct(post)
	if err != nil {
		return ErrFieldsIsEmpty.Error(), ErrFieldsIsEmpty
	}

	post.Title = TrimSpaces(post.Title)
	post.Text = TrimSpaces(post.Text)

	postID, err := s.repo.CreatePost(ctx, post)
	if err != nil {
		return "", err
	}

	return postID, nil
}

func (s *Service) GetPostByID(ctx context.Context, id string) (*Post, error) {
	Logg("logic", "GetPostByID")

	byID, err := s.repo.GetPostByID(ctx, id)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return byID, nil
}

func (s *Service) GetPosts(ctx context.Context) (*[]Post, error) {
	Logg("logic", "GetPosts")

	posts, err := s.repo.GetPosts(ctx)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

// GetAuthorPosts Done!
func (s *Service) GetAuthorPosts(ctx context.Context, aId string) (*[]Post, error) {
	Logg("logic", "GetAuthorPosts")

	posts, err := s.repo.GetAuthorPosts(ctx, aId)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (s *Service) DeletePostByID(ctx context.Context, id int) (string, error) {
	Logg("logic", "DeletePostByID")

	panic("implement me")
}

func (s *Service) UpdatePostByID(ctx context.Context, id string, post *Post) (string, error) {
	Logg("logic", "UpdatePostByID")

	byID, err := s.repo.UpdatePostByID(ctx, id, post)
	if err != nil {
		return "", err
	}

	return byID, nil
}

// UploadFile Done!
func (s *Service) UploadFile(ctx context.Context, filePath, postID, authorID string) (string, error) {
	Logg("logic", "UploadFile")

	file, err := s.repo.UploadFile(ctx, filePath, postID, authorID)
	if err != nil {
		return "", err
	}

	return file, nil
}
