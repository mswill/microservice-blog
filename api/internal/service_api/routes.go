package service_api

import "github.com/gofiber/fiber/v2"

func (h *Handler) PublicRoutes(app *fiber.App) {
	Logg("routes", "PublicRoutes")

	//apiPublic := app.Group("public")

}
func (h *Handler) PrivateRoutes(app *fiber.App) {
	Logg("routes", "PrivateRoutes")

	apiPrivate := app.Group("private")

	apiPrivate.Use(h.ParseToken)

	apiPrivate.Post("/post", h.CreatePost)
	apiPrivate.Get("/author-posts", h.GetAuthorPosts)
	apiPrivate.Get("/post/:id", h.GetPostByID)

	apiPrivate.Post("/upload", h.UploadFile)
	apiPrivate.Put("/update-post/:id", h.UpdatePostByID)
	apiPrivate.Delete("/delete-post/:id", h.DeletePostByID)
}
func (h *Handler) NotFoundRoute(app *fiber.App) {
	Logg("routes", "NotFoundRoute")

	app.Use(
		func(c *fiber.Ctx) error {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": true,
				"msg":   "sorry, endpoint is not found",
			})
		},
	)
}
