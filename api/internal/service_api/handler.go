package service_api

import (
	"context"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/segmentio/kafka-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"mime/multipart"
	"strings"
)

type Handler struct {
	service   IService
	kafkaConn *kafka.Conn
}

func NewHandler(service IService, kConn *kafka.Conn) *Handler {
	return &Handler{service: service, kafkaConn: kConn}
}

type InputData struct {
	Title    string `json:"title"`
	Text     string `json:"text"`
	ImageUrl string `json:"imageUrl,omitempty"`
}

var postIDChan = make(chan string, 1)

func (h *Handler) CreatePost(ctx *fiber.Ctx) error {
	Logg("handler", "CreatePost")

	var p Post
	var in InputData
	err := ctx.BodyParser(&in)
	if err != nil {
		fmt.Println("err -> ", err)
		return err
	}
	fmt.Println("getting -> ", in)

	user := ctx.Context().Value("user")
	userCtx, i := user.(*AuthorFromCtx)
	if !i {
		_ = ctx.JSON(fiber.Map{
			"token exp err": "Relogin please",
		})
		return nil
	}

	p.Title = in.Title
	p.Text = in.Text
	p.ImageURL = in.ImageUrl
	authorID, err := primitive.ObjectIDFromHex(userCtx.Id)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	p.AuthorID = authorID

	post, err := h.service.CreatePost(ctx.Context(), &p)
	if errors.Is(err, ErrFieldsIsEmpty) {
		_ = ctx.JSON(fiber.Map{
			"fields": post,
		})
		return err
	}
	if err != nil {
		return err
	}

	// send to UploadFile method
	go func() {
		postIDChan <- post
	}()

	_ = ctx.JSON(fiber.Map{
		"handler": post,
	})

	return nil
}

func (h *Handler) GetPosts(ctx *fiber.Ctx) error {
	Logg("handler", "GetPosts")

	user := ctx.Context().Value("user")
	userCtx, i := user.(*AuthorFromCtx)
	if !i {
		_ = ctx.JSON(fiber.Map{
			"token exp err": "Relogin please",
		})
		return nil
	}

	posts, err := h.service.GetPosts(ctx.Context())
	if err != nil {
		return err
	}

	_ = ctx.JSON(fiber.Map{
		"handler": "GetPosts",
		"user":    userCtx,
		"posts":   posts,
	})

	return nil
}

func (h *Handler) GetAuthorPosts(ctx *fiber.Ctx) error {
	Logg("handler", "AuthorPosts")

	authorCtx := ctx.Context().Value("user")
	au := authorCtx.(*AuthorFromCtx)

	posts, err := h.service.GetAuthorPosts(ctx.Context(), au.Id)
	if err != nil {
		fmt.Println(posts)
		return err
	}

	_ = ctx.JSON(fiber.Map{
		"getPosts": posts,
	})

	return nil
}

func (h *Handler) GetPostByID(ctx *fiber.Ctx) error {
	Logg("handler", "GetPostByID")

	id := ctx.Params("id", "")

	fmt.Println("ID ", id)

	byID, err := h.service.GetPostByID(ctx.Context(), id)

	if err == mongo.ErrNoDocuments {
		fmt.Println("post not found")
		return err
	}
	if err != nil {
		fmt.Println(err)
		return err
	}

	_ = ctx.JSON(fiber.Map{
		"getPostById": byID,
	})

	return nil
}

func (h *Handler) UpdatePostByID(ctx *fiber.Ctx) error {
	Logg("handler", "UpdatePostByID")

	var p Post

	id := ctx.Params("id")
	err := ctx.BodyParser(&p)

	if err != nil {
		return err
	}

	byID, err := h.service.UpdatePostByID(ctx.Context(), id, &p)
	if err != nil {
		return err
	}

	_ = ctx.JSON(fiber.Map{
		"postUpdated": byID,
	})

	return nil
}

func (h *Handler) DeletePostByID(ctx *fiber.Ctx) error {
	Logg("handler", "DeletePostByID")

	_ = ctx.JSON(fiber.Map{
		"handler": "DeletePostByID",
	})

	return nil
}

func (h *Handler) NotFoundPost(ctx *fiber.Ctx) error {
	Logg("handler", "NotFoundPost")

	ctx.Status(fiber.StatusNotFound)
	_ = ctx.JSON(fiber.Map{
		"404": "Page Not Found",
	})

	return nil
}

func (h *Handler) UploadFile(ctx *fiber.Ctx) error {
	Logg("handler", "UploadFile")

	var postId string
	go func() {
		postId += <-postIDChan
	}()
	file, err := ctx.FormFile("image")
	if err != nil {
		fmt.Println(err)
		return err
	}

	user := ctx.Context().Value("user")
	u := user.(*AuthorFromCtx)
	fmt.Println("USER ------------ ", u)

	fName := strings.Split(file.Filename, ".")[0]
	fExt := strings.Split(file.Filename, ".")[1]
	fileName := fmt.Sprintf("%s_%s.%s", u.Email, fName, fExt)

	// send dir_name to file_server
	h.sendDataToFileServerKeyValue("dir_name", u.Email)
	// send image_name to file_server
	h.sendDataToFileServerKeyValue("image_name", fileName)
	// Send file to file_server
	h.sendImageToKafka(file)

	filePath := fmt.Sprintf("%s/%s/%s", "http://localhost:12000/static", u.Email, fileName)
	fmt.Println(filePath)
	uploadFile, err := h.service.UploadFile(context.Background(), filePath, postId, u.Id)
	if err != nil {
		return err
	}
	fmt.Println("uploadFile ---- ", uploadFile)
	fmt.Println("filePath  ---- ", filePath)

	return nil
}

func (h *Handler) sendDataToFileServerKeyValue(key, val string) {
	msg := kafka.Message{Key: []byte(key), Value: []byte(val)}
	_, err := h.kafkaConn.WriteMessages(msg)
	if err != nil {
		fmt.Println("sendDataToFileServerKeyValue ", err)
		return
	}
	fmt.Println("MSG ---- ", msg)
}
func (h *Handler) sendImageToKafka(file *multipart.FileHeader) {
	Logg("handler", "sendImageToKafka")

	buf := make([]byte, file.Size)

	open, err := file.Open()
	_, err = open.Read(buf)

	if err != nil {
		return
	}
	defer open.Close()
	if err != nil {
		return
	}

	msg := kafka.Message{
		Key:   []byte("image_file"),
		Value: buf,
	}
	_, err = h.kafkaConn.WriteMessages(msg)
	if err != nil {
		fmt.Println("Write Message err kafka")
	}
	fmt.Println("MSG KEY ---- ", msg.Key)
	fmt.Println("MSG VALUE ---- ", msg.Value[0:20])
}
