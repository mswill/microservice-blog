package service_api

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
	"time"
)

var _ IRepository = &Repository{}

const (
	database    = "micro-blog"
	collPosts   = "posts"
	collAuthors = "authors"
)

type Repository struct {
	db *mongo.Client
}

func NewRepository(db *mongo.Client) *Repository {
	return &Repository{db: db}
}

// CreatePost Done!
func (r *Repository) CreatePost(ctx context.Context, post *Post) (string, error) {
	Logg("repository", "CreatePost")
	//db.posts.update({"authorId": ObjectId("626bc3d23cbdbe06f40313d1")}, {$push: {"posts": "5554444"}})

	p := bson.M{
		"title":     post.Title,
		"text":      post.Text,
		"imageUrl":  post.ImageURL,
		"authorId":  post.AuthorID,
		"isMy":      post.IsMy,
		"createdAt": time.Now(),
	}

	postID, err := r.db.Database(database).Collection(collPosts).InsertOne(ctx, p)
	if err != nil {
		fmt.Println(err)
		return "insert post error", err
	}
	id := bson.M{"_id": post.AuthorID}
	up := bson.M{"$push": bson.M{"posts": postID.InsertedID.(primitive.ObjectID).Hex()}}
	_, err = r.db.Database(database).Collection(collAuthors).UpdateOne(ctx, id, up)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return postID.InsertedID.(primitive.ObjectID).Hex(), nil
}

func (r *Repository) GetPostByID(ctx context.Context, id string) (*Post, error) {
	Logg("repository", "GetPostByID")

	var p Post
	_id, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": _id}
	findOne := r.db.Database(database).Collection(collPosts).FindOne(ctx, filter)

	err := findOne.Decode(&p)
	if err != nil {
		fmt.Println("Err to decode ", err)
		return nil, err
	}

	fmt.Println("find post ", p)

	return &p, nil
}

func (r *Repository) GetPosts(ctx context.Context) (*[]Post, error) {
	Logg("repository", "GetPosts")

	var pts []Post
	find, err := r.db.Database(database).Collection(collPosts).Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	for find.Next(ctx) {
		var p Post
		err := find.Decode(&p)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		pts = append(pts, p)
	}

	return &pts, nil
}

// GetAuthorPosts Done!
func (r *Repository) GetAuthorPosts(ctx context.Context, aId string) (*[]Post, error) {
	Logg("repository", "GetAuthorPosts")

	var i AuthorFromCtx
	aParse, _ := primitive.ObjectIDFromHex(aId)
	findAuthors := bson.M{"_id": aParse}
	err := r.db.Database(database).Collection(collAuthors).FindOne(ctx, findAuthors).Decode(&i)
	if err != nil {
		return nil, err
	}

	aid, _ := primitive.ObjectIDFromHex(i.Id)
	fiter := bson.M{"authorId": aid}
	find, err := r.db.Database(database).Collection(collPosts).Find(ctx, fiter)
	if err != nil {
		return nil, err
	}

	var pts []Post

	for find.Next(ctx) {
		var p Post
		err := find.Decode(&p)
		if err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
		pts = append(pts, p)
	}

	return &pts, nil
}

func (r *Repository) DeletePostByID(ctx context.Context, id int) (string, error) {
	Logg("repository", "DeletePostByID")
	panic("implement me")
}

func (r *Repository) UpdatePostByID(ctx context.Context, id string, post *Post) (string, error) {
	Logg("repository", "UpdatePostByID")

	fmt.Println(post)
	_id, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": _id}
	update := bson.M{"$set": bson.M{"title": post.Title, "text": post.Text}}

	_, err := r.db.Database(database).Collection(collPosts).UpdateOne(ctx, filter, update)
	if err != nil {
		return "", err
	}

	return "post updated", nil
}

// UploadFile Done!
func (r *Repository) UploadFile(ctx context.Context, filePath, postID, authorID string) (string, error) {
	Logg("repository", "UploadFile")

	var p Post
	tt, _ := primitive.ObjectIDFromHex(postID)
	pID := bson.M{"_id": tt}
	pUpdate := bson.M{"$set": bson.M{"imageUrl": filePath}}
	err := r.db.Database(database).Collection(collPosts).FindOne(ctx, pID).Decode(&p)
	if p.AuthorID.Hex() == authorID {
		_, err = r.db.Database(database).Collection(collPosts).UpdateOne(ctx, pID, pUpdate)
		if err != nil {
			return "", err
		}
	}

	return "file uploaded", nil
}
