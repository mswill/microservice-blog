package service_api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Post struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Title     string             `json:"title" validate:"required,min=3"`
	Text      string             `json:"text" validate:"required,min=3"`
	ImageURL  string             `json:"image_url"`
	AuthorID  primitive.ObjectID `json:"authorId"`
	CreatedAt time.Time          `json:"created_at"`
	IsMy      bool               `json:"is_my"`
}

type IRepository interface {
	CreatePost(ctx context.Context, post *Post) (string, error)
	GetPostByID(ctx context.Context, id string) (*Post, error)
	GetPosts(ctx context.Context) (*[]Post, error)
	GetAuthorPosts(ctx context.Context, aId string) (*[]Post, error)
	DeletePostByID(ctx context.Context, id int) (string, error)
	UpdatePostByID(ctx context.Context, id string, post *Post) (string, error)
	UploadFile(ctx context.Context, filePath, postID, authorID string) (string, error)
}
