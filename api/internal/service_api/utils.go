package service_api

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func Logg(file, method string) {
	logrus.Infof(" [ %s - %s ] ", file, method)
}

func CreateDir(ctx *fiber.Ctx) (string, error) {
	Logg("utils", "CreateDir")

	pathName := "./uploads"

	user := ctx.Context().Value("user")
	dirName := user.(*AuthorFromCtx).Email
	fmt.Println(user)
	fmt.Println("Perm mode -> ", os.ModePerm)

	err := os.Mkdir(path.Join(pathName, dirName), os.ModePerm)
	if err != nil && !os.IsExist(err) {
		fmt.Println("here ---> ", err.Error())
		logrus.Fatal(err)
	}

	abs, err := filepath.Abs(path.Join(pathName, dirName))
	if err != nil {
		fmt.Println("filepath.Abs ", err)
		return "", err
	}

	return abs, nil
}

func TrimSpaces(str string) string {
	return strings.TrimSpace(str)
}
