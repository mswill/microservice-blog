package internal

import (
	"github.com/segmentio/kafka-go"
	"os"
	"time"
)

var (
	KafkaTopics = []string{"sendFileTopic"}
	KafkaHost   = os.Getenv("KAFKA_HOST")
)

func StartKafka() *kafka.Reader {

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        []string{KafkaHost},
		Partition:      0,
		Topic:          KafkaTopics[0],
		MinBytes:       5e3,
		MaxBytes:       20e6,
		MaxWait:        time.Second * 10,
		CommitInterval: time.Second,
	})

	return r
}
