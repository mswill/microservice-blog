package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

type Server struct {
	srv  *fiber.App
	port string
}

func NewServer(srv *fiber.App, port string) *Server {
	return &Server{srv: srv, port: port}
}

func (s *Server) Run() error {

	s.srv.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Accept-Language, Content-Length, Authorization",
	}))
	s.srv.Use(compress.New())
	s.srv.Use(logger.New())
	s.srv.Use(recover.New())

	err := s.srv.Listen(":" + s.port)

	return err
}
func (s *Server) ShutDown() error {
	err := s.srv.Shutdown()
	if err != nil {
		return err
	}

	return err
}
