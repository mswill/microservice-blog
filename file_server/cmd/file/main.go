package main

import (
	"context"
	"file_server/internal"
	"file_server/internal/server"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/signal"
	"path"
	"sync"
	"syscall"
)

func main() {

	kafkaReader := internal.StartKafka()
	wg := &sync.WaitGroup{}

	errChan := make(chan error)
	signalErr := make(chan os.Signal)
	app := fiber.New()
	serve := server.NewServer(app, os.Getenv("SERVER_PORT"))

	app.Static("/static", "./uploads", fiber.Static{
		Compress: true,
	})

	wg.Add(1)
	go func() {
		fmt.Println("----------------")
		fmt.Println("")
		fmt.Println(" file_server PORT ", os.Getenv("SERVER_PORT"))
		fmt.Println("")
		fmt.Println("----------------")
		err := serve.Run()
		signal.Notify(signalErr, syscall.SIGTERM, syscall.SIGINT)
		errChan <- err
		defer wg.Done()
	}()

	wg.Add(1)
	go func() {
		err := serve.ShutDown()
		if err != nil {
			logrus.Fatal(err)
		}
		fmt.Printf(" err %#+v ", <-signalErr)
		fmt.Printf(" err %#+v ", <-errChan)
		defer wg.Done()
	}()

	// -------------- KAFKA --------------
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer func(kafkaReader *kafka.Reader) {
			err := kafkaReader.Close()
			if err != nil {
				logrus.Fatal("KAFKA ", err)
			}
		}(kafkaReader)

		var dirPath string
		var imageName string
		var fileBuf []byte
		err := kafkaReader.SetOffset(-2)
		if err != nil {
			fmt.Println("offset err ", err)
			return
		}
		fmt.Println("STATS ---> ", kafkaReader.Stats())
		for {
			message, err := kafkaReader.ReadMessage(context.Background())
			if err != nil {
				fmt.Println("Some message called error ", err.Error())
				continue
			}
			if string(message.Key) == "dir_name" {
				dirPath = CreateDir(string(message.Value))
				fmt.Println("--------------------- ", dirPath)
			}
			if string(message.Key) == "image_name" {
				imageName = string(message.Value)
				fmt.Println("--------------------- ", imageName)
			}
			if string(message.Key) == "image_file" {
				fileBuf = message.Value
				CreateImageFile(dirPath, imageName, fileBuf)
				fmt.Println("--------------------- ", fileBuf[0:20])
			}
		}
	}()
	wg.Wait()
}

func CreateDir(dirName string) string {
	uploadsDirPath := "uploads"

	err := os.Mkdir(uploadsDirPath, os.ModePerm)
	if err != nil && !os.IsExist(err) {
		fmt.Println(err)
		return ""
	}
	err = os.Mkdir(path.Join(uploadsDirPath, dirName), os.ModePerm)
	if err != nil && !os.IsExist(err) {
		fmt.Println(err)
		return ""
	}
	return path.Join(uploadsDirPath, dirName)
}

func CreateImageFile(dirPath, imageName string, file []byte) {
	err := ioutil.WriteFile(dirPath+"/"+imageName, file, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return
	}
}
