cp:
	docker container prune

delall: cp

stop:
	docker-compose down --remove-orphans

prod:
	docker-compose up --build


dev:
	docker-compose -f docker-compose-dev.yml up --build

prod:
	docker-compose up --build
