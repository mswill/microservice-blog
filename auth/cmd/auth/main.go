package main

import (
	"auth/internal/service_auth"
	"auth/pkg/db/mongodb"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"sync"
)

func main() {
	// wait group
	wg := sync.WaitGroup{}

	// app instance
	app := fiber.New()

	// monitor
	app.Get("/dash", monitor.New())

	// middleware
	app.Use(logger.New())
	app.Use(compress.New())
	app.Use(recover.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Accept-Language, Content-Length, Authorization",
	}))
	// Server
	appServer := service_auth.Server{}

	// connect to mongo
	client := mongodb.ConnectToMongo()

	repo := service_auth.NewRepository(client)
	service := service_auth.NewService(repo)
	handler := service_auth.NewHandler(service)

	handler.PublicRoutes(app)
	handler.NotFoundRoute(app)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		appServer.Run(app, wg)
		wg.Done()
	}(&wg)

	wg.Wait()
}
