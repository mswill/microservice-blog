package service_auth

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Post struct {
	ID primitive.ObjectID `json:"id"`
}
type Author struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Name      string             `json:"name" validate:"required,min=3"`
	Email     string             `json:"email" validate:"required,email"`
	Password  string             `json:"password" validate:"required,min=6"`
	CreatedAt time.Time          `json:"created_at"`
	Posts     []Post             `json:"posts"`
	IsLogged  bool               `json:"isLogged"`
}

type IRepository interface {
	SignUp(ctx context.Context, a *Author) (string, error)
	SignIn(ctx context.Context, email, password string) (*Author, error)
	Logout(ctx context.Context, id string) (string, error)
	CheckEmailUser(ctx context.Context, email string) (bool, error)
}
