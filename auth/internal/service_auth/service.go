package service_auth

import "context"

type IService interface {
	SignUp(ctx context.Context, a *Author) (string, error)
	SignIn(ctx context.Context, email, password string) (ResType, error)
	Logout(ctx context.Context, id string) (string, error)
	GenerateToken(ctx context.Context, a *Author) (string, error)
	ParseToken(ctx context.Context, token string) (*Author, error)
}
