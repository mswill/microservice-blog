package service_auth

import (
	"github.com/sirupsen/logrus"
	"strings"
)

func Logg(file, method string) {
	logrus.Infof(" [ %s - %s ] ", file, method)
}

func TrimAndToLowerCaseString(str string) string {
	Logg("utils", "TrimAndToLowerCaseString")

	return strings.ToLower(strings.TrimSpace(str))
}

func CheckHash(hash, password string) bool {
	Logg("utils", "CheckHash")

	if hash == password {
		return true
	}

	return false
}
