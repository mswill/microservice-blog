package service_auth

import "github.com/gofiber/fiber/v2"

func (h *Handler) PublicRoutes(app *fiber.App) {
	Logg("routes", "PublicRoutes")

	auth := app.Group("auth")
	auth.Post("/signup", h.SignUp)
	auth.Post("/signin", h.SignIn)
	auth.Post("/logout", h.Logout)
}

func (h *Handler) NotFoundRoute(app *fiber.App) {
	Logg("routes", "PublicRoutes")

	app.Use(
		func(ctx *fiber.Ctx) error {

			return ctx.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": true,
				"msg":   "sorry, endpoint is not found",
			})

		})
}
