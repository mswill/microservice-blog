package service_auth

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type Server struct {
}

func (s *Server) SetConfig() *fiber.Config {
	Logg("server", "SetConfig")

	cfg := fiber.Config{
		AppName:      "microservice blog",
		BodyLimit:    1024 * 1024 * 20,
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 5,
	}

	return &cfg
}

func (s *Server) Run(app *fiber.App, wg *sync.WaitGroup) {
	Logg("server", "Run")

	// defines
	errChan := make(chan error, 1)
	signalErr := make(chan os.Signal, 1)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		fmt.Println("----------------")
		fmt.Println("")
		fmt.Println(" service_auth PORT ", os.Getenv("SERVER_PORT"))
		fmt.Println("")
		fmt.Println("----------------")
		err := app.Listen(":" + os.Getenv("SERVER_PORT"))
		//err := app.Listen(":9002")1
		signal.Notify(signalErr, syscall.SIGTERM, syscall.SIGINT)
		errChan <- err
		wg.Done()
	}(wg)

	go func(app *fiber.App, wg *sync.WaitGroup) {
		err := app.Shutdown()
		if err != nil {
			fmt.Println("Fiber shutdown error")
		}
		fmt.Printf(" err %#+v ", <-signalErr)
		fmt.Printf(" err %#+v ", <-errChan)

		wg.Done()
	}(app, wg)
}
