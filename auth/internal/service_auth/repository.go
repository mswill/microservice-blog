package service_auth

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

var _ IRepository = &Repository{}

const (
	dbName  = "micro-blog"
	colName = "authors"
)

var (
	NotFoundErr     = errors.New("User not found ")
	NotFoundLoggin  = errors.New("Email is not registered ")
	AuthorExistsErr = errors.New("Email already exists. Please use different email. ")
)

type Repository struct {
	db *mongo.Client
}

func NewRepository(db *mongo.Client) *Repository {
	return &Repository{db: db}
}

// SignUp Done!
func (r Repository) SignUp(ctx context.Context, a *Author) (string, error) {
	Logg("repository", "SignUp")

	ok, err := r.CheckEmailUser(ctx, a.Email)

	if err == AuthorExistsErr {
		return err.Error(), err
	}

	if ok {
		fmt.Println("here ")
		author := bson.M{
			"name":      a.Name,
			"email":     a.Email,
			"password":  a.Password,
			"createdAt": a.CreatedAt,
			"posts":     a.Posts,
			"isLogged":  a.IsLogged,
		}
		_, err := r.db.Database(dbName).Collection(colName).InsertOne(ctx, author)
		if err != nil {
			return "", err
		}
		return "success", nil
	}

	if err != nil {
		return err.Error(), err
	}

	return "error signup", nil
}

// SignIn Done!
func (r Repository) SignIn(ctx context.Context, email, password string) (*Author, error) {
	Logg("repository", "SignIn")

	var a Author

	up := bson.M{"$set": bson.M{"isLogged": true}}
	find := bson.M{"email": email, "password": password}
	_, err := r.db.Database(dbName).Collection(colName).UpdateOne(ctx, find, up)
	if err != nil {
		fmt.Println(err.Error())
		return &a, err
	}
	err = r.db.Database(dbName).Collection(colName).FindOne(ctx, find).Decode(&a)

	if err == mongo.ErrNoDocuments {
		return &a, NotFoundLoggin
	}

	return &a, nil
}

func (r Repository) Logout(ctx context.Context, id string) (string, error) {
	Logg("repository", "Logout")

	_id, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": _id}
	update := bson.M{"$set": bson.M{"isLogged": false}}
	one, err := r.db.Database(dbName).Collection(colName).UpdateOne(ctx, filter, update)

	fmt.Println(" one.ModifiedCount ", one.ModifiedCount)

	if err != nil {
		return "err", err
	}

	return "logout", nil
}

// CheckEmailUser Done
func (r *Repository) CheckEmailUser(ctx context.Context, email string) (bool, error) {
	Logg("repository", "CheckEmailUser")

	var a Author

	f := bson.M{"email": email}
	found := r.db.Database(dbName).Collection(colName).FindOne(ctx, f)
	err := found.Decode(&a)

	if err == mongo.ErrNoDocuments {
		return true, NotFoundErr
	}

	if err != nil {
		return false, fmt.Errorf("could not found author err %s: ", err)
	}

	if a.Email == email {
		return false, AuthorExistsErr
	}

	return false, nil
}
