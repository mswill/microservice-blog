package service_auth

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
	"time"
)

type Handler struct {
	service IService
}

func NewHandler(service IService) *Handler {
	return &Handler{service: service}
}

type InputDataReg struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
type InputDataLog struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SignUp Done
func (h *Handler) SignUp(ctx *fiber.Ctx) error {
	Logg("handler", "SignUp")

	var in InputDataReg
	var a Author

	err := ctx.BodyParser(&in)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	a.Email = in.Email
	a.Name = in.Name
	a.Password = in.Password
	a.CreatedAt = time.Now().UTC().Local()

	success, err := h.service.SignUp(ctx.Context(), &a)

	if err == AuthorExistsErr {
		ctx.Status(fiber.StatusNotFound)
		_ = ctx.JSON(fiber.Map{
			"msg":    success,
			"status": fiber.StatusNotFound,
		})
		return nil
	}

	if err != nil {
		_ = ctx.JSON(fiber.Map{
			"err": true,
			"msg": err.Error(),
		})

		return nil
	}

	_ = ctx.JSON(fiber.Map{
		"signUp": success,
		"status": fiber.StatusCreated,
	})

	return nil
}

// SignIn Done!
func (h *Handler) SignIn(ctx *fiber.Ctx) error {
	Logg("handler", "SignIn")

	var in InputDataLog
	err := ctx.BodyParser(&in)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	authorResponse, err := h.service.SignIn(ctx.Context(), in.Email, in.Password)

	if err == NotFoundLoggin {
		_ = ctx.JSON(fiber.Map{
			"signIn": err.Error(),
		})
		return nil
	}
	if err != nil {
		fmt.Println(err)
		return err
	}
	ctx.Set("token", authorResponse.token)

	_ = ctx.JSON(fiber.Map{
		"signin_response": authorResponse,
		"token":           authorResponse.token,
	})

	return nil
}

func (h *Handler) Logout(ctx *fiber.Ctx) error {
	Logg("handler", "Logout")

	id := struct {
		Id string `json:"id"`
	}{}

	err := ctx.BodyParser(&id)
	if err != nil {
		fmt.Println("body parse err ", err)
		return err
	}

	logout, err := h.service.Logout(ctx.Context(), id.Id)
	
	if err != nil {
		return err
	}

	_ = ctx.JSON(fiber.Map{
		"logout": logout,
	})

	return nil
}

func Check() error {
	a := struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}{
		Email:    "ser@gmail.com",
		Password: "123",
	}

	marshal, err := json.Marshal(&a)
	if err != nil {
		return err
	}

	cl := &fasthttp.Client{}
	req := fasthttp.AcquireRequest()
	req.Header.Set("Content-Type", "application/json")
	req.Header.SetMethod(fiber.MethodPost)
	req.SetBody(marshal)
	//req.SetBodyString("hello")

	//req.SetRequestURIBytes(marshal)
	defer fasthttp.ReleaseRequest(req)

	req.SetRequestURI("http://localhost:9001/private/post")
	err = cl.Do(req, nil)
	if err != nil {
		return err
	}
	return nil
}
