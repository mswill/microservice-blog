package service_auth

import (
	"context"
	"crypto/sha1"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v4"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

var _ IService = &Service{}

const (
	salt    = "jdhfh4j55"
	signKey = "tokenSecretdjkfhgfdg"
)

type Service struct {
	repo IRepository
}

func NewService(repo IRepository) *Service {
	return &Service{repo: repo}
}

type ResType struct {
	Author
	token string `json:"token"`
}

// SignUp Done!
func (s Service) SignUp(ctx context.Context, a *Author) (string, error) {
	Logg("logic", "SignUp")

	v := validator.New()
	a.Name = TrimAndToLowerCaseString(a.Name)
	a.Email = TrimAndToLowerCaseString(a.Email)
	a.Password = strings.TrimSpace(a.Password)
	a.Posts = []Post{}
	err := v.Struct(a)
	if err != nil {
		return err.Error(), err
	}

	a.Password = generateHashToPassword(a.Password)

	success, err := s.repo.SignUp(ctx, a)
	return success, err
}

// SignIn Done!
func (s Service) SignIn(ctx context.Context, email, password string) (ResType, error) {
	Logg("logic", "SignIn")

	var res ResType
	email = TrimAndToLowerCaseString(email)
	password = strings.TrimSpace(password)

	hash := generateHashToPassword(password)

	in, err := s.repo.SignIn(ctx, email, hash)
	if err != nil {
		return res, err
	}

	token, err := s.GenerateToken(ctx, in)
	if err != nil {
		return res, err
	}

	res.ID = in.ID
	res.Name = in.Name
	res.Email = in.Email
	res.token = token
	res.IsLogged = in.IsLogged

	return res, nil
}

func (s Service) Logout(ctx context.Context, id string) (string, error) {
	Logg("logic", "Logout")
	
	logout, err := s.repo.Logout(ctx, id)
	if err != nil {
		return "", err
	}

	return logout, nil
}

// GenerateToken Done!
func (s Service) GenerateToken(ctx context.Context, a *Author) (string, error) {
	Logg("logic", "GenerateToken")

	t := jwt.New(jwt.SigningMethodHS256)
	claims := t.Claims.(jwt.MapClaims)

	claims["isLogged"] = a.IsLogged
	claims["id"] = a.ID
	claims["email"] = a.Email
	claims["name"] = a.Name
	claims["exp"] = time.Now().Add(time.Hour * 24 * 2).Unix()
	token, err := t.SignedString([]byte(signKey))
	if err != nil {
		logrus.Fatal(err)
		return "creation err token ", err
	}

	return token, nil
}

func (s Service) ParseToken(ctx context.Context, token string) (*Author, error) {
	Logg("logic", "ParseToken")

	parse, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(("Invalid Signing Method"))
		}
		return []byte(signKey), nil
	})

	if claims, ok := parse.Claims.(jwt.MapClaims); ok && parse.Valid {
		fmt.Println(claims["id"])
		fmt.Println(claims["email"])
		fmt.Println(claims["name"])
		fmt.Println(claims["isLogged"])
		fmt.Println(claims["exp"])
	} else {
		fmt.Println(err)
	}
	if err != nil {
		return nil, err
	}

	fmt.Println("PARSE ----> ")

	return &Author{}, nil
}

// Done!
func generateHashToPassword(password string) string {

	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}
