package mongodb

import (
	"auth/internal/service_auth"
	"context"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

func ConnectToMongo() *mongo.Client {
	service_auth.Logg("pkg/db/mongodb/mongo", "ConnectToMongo")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_PATH")))
	//client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))

	if err != nil {
		logrus.Fatalln(err)
	}

	return client
}
