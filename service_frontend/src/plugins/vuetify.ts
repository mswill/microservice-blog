// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import "@fortawesome/fontawesome-free/css/all.css"
import "material-design-icons-iconfont/dist/material-design-icons.min.css"
import { aliases, fa } from 'vuetify/lib/iconsets/fa'
import { mdi } from 'vuetify/lib/iconsets/mdi'
// Vuetify
import { createVuetify } from 'vuetify'

export default createVuetify(
	{
		icons: {
			defaultSet: 'fa' || 'mdi',
			aliases,
			sets: {
				fa,
				mdi,
			}
		},
	}
)

