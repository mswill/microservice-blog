import { defineStore } from "pinia";
import { postService } from "@/services/post.service";

export type  postsType = [
	{
		authorId: string
		created_at: string
		id: string
		image_url: string
		is_my: boolean
		text: string
		title: string
	}
]
export type postType = {
	authorId: string
	created_at: string
	id: string
	image_url: string
	is_my: boolean
	text: string
	title: string
}


export const usePostStore = defineStore('postStore', {
	state: () => ({
		posts: [] as postsType | [],
		post: {} as postType

	}),
	actions: {
		async createPost(postData: any) {
			await postService.createPost(postData)
		},
		async fetchPosts() {
			this.posts = await postService.fetchPosts();
		},
		async fetchPostById(id: string) {
			this.post = await postService.getPostByID(id)
		},
		async postUpdate (data:{title: string, text: string}, id:string) {
			const postUpdated = await postService.updatePostByID(data,id)
		}
	},
	getters: {
		getPosts: state => state.posts,
		getPostById: state => state.post,
	}
})
