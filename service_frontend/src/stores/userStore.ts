import { defineStore } from 'pinia'
import { localStorageService } from '@/services/localStorage.service';
import { authService } from '@/services/auth.service';
import type { UserData } from "@/services/types/user.types";


export type userStoreType = { name: string, email: string, isLogged: boolean, id: string } | null

export const useUserStore = defineStore('userStore', {
	state: () => ({
		user: null as userStoreType,
		isLogged: false,
		regNotify: false as boolean | string,
		logNotify: '' as string
	}),


	actions: {
		async resetUser() {
			this.user = null
			await localStorageService.clearUserFromLocalStore()
			await localStorageService.clearTokenFromLocalStorage()
		},
		async resetNotify(reset: boolean) {
			this.regNotify = !reset
		},
		async signUP(data: UserData) {
			const res = await authService.signUp(data);
			if (res.msg) {
				this.regNotify = res.msg;
				return
			}
			this.regNotify = res.signUp
		},
		async signIN(dataLog: any) {
			const res = await authService.signIn(dataLog);
			this.user = res.signin_response
			await localStorageService.setUserToLocalStorage(this.user)
			this.logNotify = res.signIn
		},
		async setUserToStore(user: any) {
			this.user = user
		},
		async logout() {
			// @ts-ignore
			await authService.logout(this.user)
		},
	},
	getters: {
		getUser: state => state.user,
		getUserName: state => state.user?.name,
		getNotify: state => state.regNotify,
	}
})
