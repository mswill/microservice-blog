import { useUserStore } from '@/stores/userStore';

type userToLocalStore = {
	name: string,
	email: string,
	isLogged: boolean,
	id: string
}

class LocalStorageService {

	async setTokenToLocalStorage(token:string) {
		await localStorage.setItem('token', token)
	}
	async getTokenFromLocalStorage(): Promise<string> {
		const token = localStorage.getItem('token')
		if (token === undefined) {
			return ''
		}

		return token!
	}
	async clearTokenFromLocalStorage() {
		localStorage.removeItem('token')
	}
	async clearUserFromLocalStore () {
		localStorage.removeItem('user')
	}
	async setUserToLocalStorage(user: userToLocalStore | null) {
		localStorage.setItem('user', JSON.stringify(user))
	}
	async getUserFromLocalStorage() {
		const uS = useUserStore();
		const userFromLocalStore = localStorage.getItem('user');
		if (userFromLocalStore === undefined) {
			return
		}
		// @ts-ignore
		await uS.setUserToStore(JSON.parse(userFromLocalStore))

	}
}

const localStorageService = new LocalStorageService()

export {
	localStorageService
}
