import axios from "axios";
import type { UserData } from "@/services/types/user.types";
import { localStorageService } from "@/services/localStorage.service";


export interface resType {
	signUp: string,
	msg: string
}

class AuthService {
	private lStorage = localStorageService
	private authApi = `http://localhost:11000/auth`;
	private regUrl = 'signup';
	private logUrl = 'signin'

	async signUp(userData: any): Promise<resType> {
		try {
			const res = await axios.post(`${ this.authApi }/${ this.regUrl }`, userData, {
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			});
			return res.data
		} catch (e: any) {
			return e.response.data
		}
	}

	async signIn(userData: any): Promise<any> {
		try {
			const res = await axios.post(`${ this.authApi }/${ this.logUrl }`, userData, {
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			});
			await this.lStorage.setTokenToLocalStorage(res.data.token)
			return res.data
		} catch (e: any) {
			return e.response.data
		}
	}

	async logout(id: any) {
		try {
			const log = await axios.post(this.authApi + '/logout', id, {
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			})
			console.log(log.data);
		} catch (e: any) {
			return e.response.data
		}
	}
}

const authService = new AuthService();

export {
	authService
}
