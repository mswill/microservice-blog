interface UserData {
	url: string
	token: string
	data: regUserType
}
interface regUserType {
	name: string,
	email: string,
	password: string
}


export type {
	UserData,
}
