import { localStorageService } from "@/services/localStorage.service";

import axios from "axios";


class PostService {
	private lStorage = localStorageService
	private path = 'http://localhost:10000/private'
	private postPath = '/post'
	private postUpload = '/upload'
	private postsFetch = '/author-posts';

	async createPost(postData: any) {
		const token = await this.lStorage.getTokenFromLocalStorage();
		const headers = {
			"Access-Control-Allow-Origin": "*",
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${ token }`,
		}
		const resP = await axios.post(this.path + this.postPath, { title: postData.title, text: postData.text },
			{
				headers
			})
		const resImg = await axios.post(this.path + this.postUpload, postData.fileImage,
			{
				headers
			})
	}

	async fetchPosts() {
		const token = await this.lStorage.getTokenFromLocalStorage();
		const headers = {
			"Access-Control-Allow-Origin": "*",
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${ token }`,
		}
		const posts = await axios.get(this.path + this.postsFetch, {
			headers
		})
		return posts.data['getPosts']
	}


	async getPostByID(id: any) {
		const token = await this.lStorage.getTokenFromLocalStorage();
		const headers = {
			"Access-Control-Allow-Origin": "*",
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${ token }`,
		}
		const postById = await axios.get(this.path + `/post/${ id }`, {
			headers
		})
		return postById.data['getPostById']
	}

	async updatePostByID(data: { title: string, text: string }, id: any) {
		const token = await this.lStorage.getTokenFromLocalStorage();
		const headers = {
			"Access-Control-Allow-Origin": "*",
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${ token }`,
		}
		const postUpdated = await axios.put(this.path + `/update-post/${ id }`, data, {
			headers
		})
		console.log(postUpdated.data);
	}

	async deletePostByID(id: any) {

	}
}


const postService = new PostService()

export {
	postService
}
