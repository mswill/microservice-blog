import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/users',
		name: 'Users',
		component: () => import('../components/UsersComponent.vue'),
	},
	{
		path: '/create',
		name: 'Create posts',
		component: () => import('../components/CreatePostComponent.vue')
	},
	{
		path: '/posts',
		name: 'Posts',
		component: () => import('../components/PostsComponent.vue')
	},
	{
		path: '/post/:id',
		name: 'Post',
		component: () => import('../components/PostComponent.vue')
	},
	{
		path: '/edit-post/:id',
		name: 'EditPost',
		component: () => import('../components/FormPostEditComponent.vue')
	},
	{
		path: "/:catchAll(.*)",
		redirect: '/users'
	},
];
const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes
})

export default router
